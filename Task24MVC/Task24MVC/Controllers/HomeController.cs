﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24MVC.Models;

namespace Task24MVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //Coach.GetSampleCoachData().Select(c=> c?.Name)
            return View("CoachInfo");
        }
        [HttpGet]

        public IActionResult CoachInfo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CoachInfo(Coach coach)
        {
            if (ModelState.IsValid)
            {
                CoachGroup.Coaches.Add(coach);

                return View("AddCoachConfirmation", coach);
            }
            else
            {
                return View();
            }

        }
        [HttpGet]
        public IActionResult AllCoaches(Coach coach)
       {

            Coach coach1 = new Coach { Name = "Sindre", ID = 1, IsAvailable = true, Level = "Beginner" };
            Coach coach2 = new Coach { Name = "Aslak Sverre", ID = 2, IsAvailable = false, Level = "Pro" };
            Coach coach3 = new Coach { Name = "Sondre", ID = 3, IsAvailable = true, Level = "Pro" };
            Coach coach4 = new Coach();

            CoachGroup.AddCoach(coach1);
            CoachGroup.AddCoach(coach2);
            CoachGroup.AddCoach(coach3);
            CoachGroup.AddCoach(coach4);
            CoachGroup.AddCoach(null);

            return View(CoachGroup.Coaches.FindAll(sCoach => sCoach?.Name[0] == 'S').Select(c =>
            {
                int? id = c?.ID ?? 9;
                string name = c?.Name ?? "No Name";
                string level = c?.Level ?? "No Level";
                bool? available = c?.IsAvailable ?? false;

                return $"ID: {id} Name: {name} Level: {level} Available?: ";
            }));
        }
    }
}
