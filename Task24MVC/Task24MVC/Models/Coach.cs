﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task24MVC.Models
{
    public class Coach
    {
        [Required(ErrorMessage = "Please enter a number")]
        [RegularExpression("^\\d+$", ErrorMessage ="Please enter a valid whole number")]
        public int? ID { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "No name here";
        [Required(ErrorMessage = "Please indicate availability")]
        public bool? IsAvailable { get; set; }

        [Required(ErrorMessage = "Please enter a valid level")]
        public string Level { get; set; } = " No level here";

    }
}
